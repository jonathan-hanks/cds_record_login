//
// Created by jonathan.hanks on 2/12/20.
//
#include "logon.hh"

#include <algorithm>
#include <fstream>
#include <sstream>
#include <chrono>
#include <mutex>
#include <thread>
#include <vector>

#include <cstdio>
#include <cerrno>
#include <cstring>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <pwd.h>

static const char* const DEFAULT_FNAME = "/var/run/cds_logins";

struct UserEntry
{
    std::string  name{ };
    std::int64_t uid{ -1 };
    int          count{ 0 };
};

class FileBasedMutex
{
public:
    explicit FileBasedMutex( std::string path )
        : path_{ std::move( path ) }, locked_{ false }
    {
    }

    FileBasedMutex( const FileBasedMutex& ) = delete;
    FileBasedMutex( FileBasedMutex&& ) = delete;
    FileBasedMutex& operator=( const FileBasedMutex& ) = delete;
    FileBasedMutex& operator=( FileBasedMutex&& ) = delete;

    bool
    try_lock( ) noexcept
    {
        if ( locked_ )
        {
            return true;
        }

        do
        {
            int fd =
                ::open( path_.c_str( ), O_CREAT | O_EXCL, S_IRUSR | S_IWUSR );
            if ( fd >= 0 )
            {
                ::close( fd );
                locked_ = true;
                return true;
            }
            if ( errno != EINTR )
            {
                return false;
            }
        } while ( true );
    }

    void
    lock( )
    {
        while ( !try_lock( ) )
        {
            std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
        }
    }

    template < class Rep, class Period >
    bool
    try_lock_for( const std::chrono::duration< Rep, Period >& duration )
    {
        auto end_time = std::chrono::steady_clock::now( ) + duration;
        while ( !try_lock( ) )
        {
            auto now = std::chrono::steady_clock::now( );
            if ( now > end_time )
            {
                return false;
            }
            std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
        }
        return true;
    }

    template < class Clock, class Duration >
    bool
    try_lock_until(
        const std::chrono::time_point< Clock, Duration >& timeout_time )
    {
        while ( !try_lock( ) )
        {
            auto now = Clock::now( );
            if ( now > timeout_time )
            {
                return false;
            }
            std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
        }
        return true;
    }

    void
    unlock( ) noexcept
    {
        if ( !locked_ )
        {
            return;
        }
        ::unlink( path_.c_str( ) );
        locked_ = false;
    }

private:
    std::string path_;
    bool        locked_;
};

UserEntry
parse_line( const std::string& line )
{
    UserEntry user;

    std::istringstream is( line );
    is >> user.name;
    is >> user.uid;
    is >> user.count;
    return user;
}

std::vector< UserEntry >
load_from_file( const std::string& fname )
{
    std::vector< UserEntry > users;

    std::fstream input( fname );
    std::string  line;
    while ( std::getline( input, line ) )
    {
        UserEntry user = parse_line( line );
        if ( !user.name.empty( ) )
        {
            users.emplace_back( std::move( user ) );
        }
    }
    return users;
}

void
increment_or_add_user( std::vector< UserEntry >& users,
                       const std::string&        username )
{
    auto it = std::find_if(
        users.begin( ), users.end( ), [ &username ]( const UserEntry& user ) {
            return user.name == username;
        } );
    if ( it != users.end( ) )
    {
        it->count++;
    }
    else
    {
        auto         pw_entry = ::getpwnam( username.c_str( ) );
        std::int64_t uid = -1;
        if ( pw_entry )
        {
            uid = static_cast< std::int64_t >( pw_entry->pw_uid );
        }
        UserEntry user;
        user.name = username;
        user.uid = uid;
        user.count = 1;
        users.emplace_back( std::move( user ) );
    }
}

void
decrement_user( std::vector< UserEntry >& users, const std::string& username )
{
    auto it = std::find_if(
        users.begin( ), users.end( ), [ &username ]( const UserEntry& user ) {
            return user.name == username;
        } );
    if ( it != users.end( ) )
    {
        it->count--;
    }
}

void
save_users( std::vector< UserEntry >& users, const std::string& fname )
{
    if ( fname.empty( ) )
    {
        return;
    }
    std::sort( users.begin( ),
               users.end( ),
               []( const UserEntry& u1, const UserEntry& u2 ) -> bool {
                   return u1.name < u2.name;
               } );
    std::string tmp_name{ fname };
    tmp_name.append( ".tmp" );
    {
        std::ofstream out( tmp_name );
        std::for_each(
            users.begin( ), users.end( ), [ &out ]( const UserEntry& user ) {
                if ( user.name.empty( ) || user.count < 1 )
                {
                    return;
                }
                out << user.name << " " << user.uid << " " << user.count
                    << "\n";
            } );
        out.flush( );
    }
    if ( std::rename( tmp_name.c_str( ), fname.c_str( ) ) != 0 )
    {
        ::unlink( tmp_name.c_str( ) );
    }
}

void
mark_login( const std::string& username, const std::string& db_fname ) noexcept
try
{
    if ( username.empty( ) || db_fname.empty( ) )
    {
        return;
    }
    std::string lock_fname = db_fname;
    lock_fname += ".lock";

    FileBasedMutex                     m_( lock_fname );
    std::unique_lock< FileBasedMutex > l_( m_, std::defer_lock );

    if ( !l_.try_lock_for( std::chrono::milliseconds( 100 ) ) )
    {
        return;
    }
    auto users = load_from_file( db_fname );
    increment_or_add_user( users, username );
    save_users( users, db_fname );
}
catch ( ... )
{
}

void
mark_logout( const std::string& username, const std::string& db_fname ) noexcept
try
{
    if ( username.empty( ) || db_fname.empty( ) )
    {
        return;
    }
    std::string lock_fname = db_fname;
    lock_fname += ".lock";

    FileBasedMutex                     m_( lock_fname );
    std::unique_lock< FileBasedMutex > l_( m_, std::defer_lock );

    if ( !l_.try_lock_for( std::chrono::milliseconds( 100 ) ) )
    {
        return;
    }
    auto users = load_from_file( db_fname );
    decrement_user( users, username );
    save_users( users, db_fname );
}
catch ( ... )
{
}

extern const char*
db_fname_from_args( int argc, char* argv[] ) noexcept
try
{
    for ( int i = 1; i < argc; ++i )
    {
        if ( std::strncmp( argv[ i ], "file=", 5 ) == 0 &&
             std::strlen( argv[ i ] ) > 5 )
        {
            return argv[ i ] + 5;
        }
    }
    return DEFAULT_FNAME;
}
catch ( ... )
{
    return DEFAULT_FNAME;
}