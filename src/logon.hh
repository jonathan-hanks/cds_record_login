//
// Created by jonathan.hanks on 2/12/20.
//

#ifndef CDS_LOGON_LOGON_HH
#define CDS_LOGON_LOGON_HH

#include <string>

extern const char* db_fname_from_args( int argc, char* argv[] ) noexcept;

extern void mark_login( const std::string& username,
                        const std::string& db_fname ) noexcept;

extern void mark_logout( const std::string& username,
                         const std::string& db_fname ) noexcept;

#endif // CDS_LOGON_LOGON_HH
