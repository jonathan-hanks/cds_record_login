
#include "logon.hh"

#include <cstdlib>

int
main( int argc, char* argv[] )
{
    auto username = std::getenv( "PAM_USER" );
    if ( username )
    {
        mark_login( username, db_fname_from_args( argc, argv ) );
    }
    return 0;
}
