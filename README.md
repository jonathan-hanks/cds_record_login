# cds_record_logon
A simple set of programs to record logins/logouts.  Writen to be used by 
pam_script to track session start/close.

Inputs from the environment:
 * PAM_USER - environment variable holding the username.
 
Usage on login:
 set PAM_USER to the user that is logging into the system.

 pam_script_ses_open
 
Usage on logout:
 set PAM_USER to the user that is logging out of the system.

 pam_script_ses_close

Files used:
 * /var/run/cds_login - a text file with lines of the format 
 "username uid count"
 * /var/run/.cds_login.lock - lock file used to ensure synchronized access to /var/run/cds_login

The process will not block (for long) and will not fail.  It is designed 
to be best effort and then to move on and not block a login process.

## Config ##

A possible config would look like:

ssh session	optional	pam_script.so dir=/usr/share/cds_record_logon
